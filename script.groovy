def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'dockerhub', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t wilfried2015/wilfried:jma-2.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push wilfried2015/wilfried:jma-2.0'
    }
} 

def pushImage () {
    echo "taging the omage en push it to ecr"
    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'aws-key', usernameVariable: 'AWS_ACCESS_KEY_ID', passwordVariable: 'AWS_SECRET_ACCESS_KEY']]) {
    AWS ("ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 482996790476.dkr.ecr.us-east-1.amazonaws.com")
    }
    sh 'docker build -t my-app .'
    sh 'docker tag my-app:latest 482996790476.dkr.ecr.us-east-1.amazonaws.com/my-app:latest'
    sh 'docker push 482996790476.dkr.ecr.us-east-1.amazonaws.com/my-app:latest'
}

def deployApp() {
    echo 'deploying the application...'
} 

return this